import java.util.Arrays;
import java.util.Objects;
import java.util.ArrayList;


public class Main {

    static String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
    static int[] hoursWorked = {35, 38, 35, 38, 40};
    static double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
    static String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

    static String searchPosition = "Perfectionniste";
    static double[] hebdoWage = new double[5];

    static ArrayList<String> employeesFound = new ArrayList<>();

    public static void main(String[] args) {

        for (int i = 0; i < employeeNames.length; i++) {
            if (hoursWorked[i] <= 35) {
                hebdoWage[i] = hoursWorked[i] * hourlyRates[i];
        } else {
                hebdoWage[i] = hourlyRates[1] * 35 + ((hoursWorked[i] - 35) * (hourlyRates[i] * 1.5));
            }
        }
        System.out.println(Arrays.toString(hebdoWage));

        for (int i = 0; i < employeeNames.length; i++) {
            if (Objects.equals(searchPosition, positions[i])) {
                employeesFound.add(employeeNames[i]);
            }
        }
        if (employeesFound.isEmpty()) {
            System.out.println("Aucun employé trouvé.");
        } else {
            System.out.println(employeesFound);
        }
    }
}